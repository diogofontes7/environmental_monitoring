from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from rest_framework.schemas import get_schema_view
from air_quality import views
from views import MonitorView, ThresholdValuesView, ToAlertView, PollutantsView, AlertView

router = DefaultRouter()
router.register(r'pollutants', views.PollutantsView)
router.register(r'threshold', views.ThresholdValuesView)
router.register(r'toalert', views.ToAlertView)
router.register(r'monitor', views.MonitorView)
router.register(r'alert', views.AlertView)

schema_view = get_schema_view(title='Pastebin API')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'api-auth/', include('rest_framework.urls', 
                              namespace='rest_framework')),
    url(r'schema/$', schema_view)
]


