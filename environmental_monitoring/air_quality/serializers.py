from rest_framework import serializers
from .models import Monitor, ToAlert, ThresholdValues, Pollutants, Alert

class ToAlertSerializer(serializers.ModelSerializer):    
    
    class Meta:
        model = ToAlert
        fields = '__all__'
        
class PollutantsSerializer(serializers.ModelSerializer):    
    
    class Meta:
        model = Pollutants
        fields = '__all__'

class ThresholdValuesSerializer(serializers.ModelSerializer):    
    
    class Meta:
        model = ThresholdValues
        fields = '__all__'

    
class MonitorSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Monitor
        fields = '__all__'
        

class AlertSerializer(serializers.ModelSerializer):    
    
    class Meta:
        model = Alert
        fields = '__all__'