from django.contrib import admin
from .models import Monitor, ToAlert, Pollutants, ThresholdValues
from leaflet.admin import LeafletGeoAdmin


#Register the models
class MonitorAdmin(LeafletGeoAdmin):
    list_display = ('value', 'timestamp', 'alert_user', 
                    'param', 'threshold', 'point')
    
admin.site.register(Monitor)
admin.site.register(ToAlert)
admin.site.register(Pollutants)
admin.site.register(ThresholdValues)