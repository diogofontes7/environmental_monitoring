from rest_framework import viewsets, filters
import django_filters.rest_framework
from rest_framework_gis.filters import InBBOXFilter
from .models import Monitor, ToAlert, Pollutants, ThresholdValues, Alert
from .serializers import MonitorSerializer, ToAlertSerializer, PollutantsSerializer, ThresholdValuesSerializer, AlertSerializer


class ToAlertView(viewsets.ModelViewSet):
    queryset = ToAlert.objects.all()
    serializer_class = ToAlertSerializer


class PollutantsView(viewsets.ModelViewSet):
    queryset = Pollutants.objects.all()
    serializer_class = PollutantsSerializer
 
    
class ThresholdValuesView(viewsets.ModelViewSet):
    queryset = ThresholdValues.objects.all()
    serializer_class = ThresholdValuesSerializer


class MonitorView(viewsets.ModelViewSet):
    queryset = Monitor.objects.all()
    serializer_class = MonitorSerializer    

class AlertView(viewsets.ModelViewSet):
    queryset = Alert.objects.all()
    serializer_class = AlertSerializer
    filter_backends = (filters.SearchFilter, InBBOXFilter,)
    search_fields = ['alert_user',]
    bbox_filter_field = 'point'
    bbox_filter_include_overlapping = True   
    