from django.contrib.gis.db import models 
from multiselectfield import MultiSelectField

class ToAlert(models.Model):
    names = [
        ('rvitorino','rvitorino'),
        ('tbatista','tbatista'),
    ]
    alert_user = models.CharField(max_length=50, unique=True, choices=names, default='rvitorino')
    
    def __str__(self):
        return self.alert_user    

class Pollutants(models.Model):
    pollutants = [
        ('CO', 'CO'),
        ('CO2', 'CO2'),
        ('SO','SO'),
    ]
    param = models.CharField(max_length=10, unique=True, choices=pollutants, 
                             default = 'CO')
    
    def __str__(self):
        return self.param    
    
class ThresholdValues(models.Model):
    threshold = models.CharField(max_length=20 ,unique=True, default=0)
    
    def __str__(self):
        return self.threshold    
    
class Monitor(models.Model):
    value = models.CharField(max_length=100, default=0) 
    timestamp = models.DateTimeField(auto_now_add=True)
    alert_user = models.ForeignKey(ToAlert, to_field='alert_user',
                             on_delete=models.CASCADE, null=True)
    param = models.ForeignKey(Pollutants, to_field='param',
                              on_delete=models.CASCADE)
    threshold = models.ForeignKey(ThresholdValues, to_field='threshold',
                                  on_delete=models.CASCADE)
    point = models.MultiPointField(srid=4326, default=None)
    
    def __str__(self):
        return self.value
    
class Alert(models.Model):
    monitor_params = models.ForeignKey(Monitor, to_field='id',
                                  on_delete=models.CASCADE, default=0)
    alert_user = models.CharField(max_length=50, default=' ')
    param = models.CharField(max_length=10, default=' ')
    value = models.CharField(max_length=100, default=' ')
    threshold = models.CharField(max_length=100, default=' ')
    timestamp = models.CharField(max_length=100, default=' ')
    point = models.MultiPointField(srid=4326, default=None)
    
    
    def save(self, *args, **kwargs):
        self.alert_user = self.monitor_params.alert_user
        self.param = self.monitor_params.param
        self.value = self.monitor_params.value
        self.threshold = self.monitor_params.threshold
        self.timestamp = self.monitor_params.timestamp
        self.point = self.monitor_params.point
        super(Alert, self).save(*args, **kwargs)