import subprocess
import random
import psycopg2
import sys
import os

def delete(table):
    '''
    This function delete the data from a table in the indicated database
    '''
    con = None
    
    try:
    
        con = psycopg2.connect(database = 'airquality_db', host = 'localhost', user = 'postgres', password = 'admin') 
        cur = con.cursor()
        cur.execute('DELETE FROM air_quality_%s;' %(table))          
        con.commit()
        
    
    except psycopg2.DatabaseError, e:
        
        if con:
            con.rollback()
        
        print 'Error %s' % e    
        sys.exit(1)
        
        
    finally:
        
        if con:
            con.close()
            
            
def get_db_data(col, table):
    '''
    This function collects data from a table in the database
    '''
    con = None
    
    try:    
        con = psycopg2.connect(database = 'airquality_db', host = 'localhost', user = 'postgres', password = 'admin') 
        cur = con.cursor()
        cur.execute('SELECT %s FROM air_quality_%s;' %(col, table))          
        rows = cur.fetchall()
        
        global db_data
        db_data = []
        for row in rows:
            db_data.append(row)
            
        cur.close()
    
    except psycopg2.DatabaseError, e:        
        if con:
            con.rollback()        
        print 'Error %s' % e    
        sys.exit(1)       
    finally:        
        if con:
            con.close()
    return db_data
    
def add_rule(user, password, param, threshold, users):
    cmd = "http -a %s:%s POST :8000/air_quality/pollutants/ param=%s" %(user, 
                                                                     password, 
                                                                     param)
    FNULL = open(os.devnull, 'w')
    subprocess.call(cmd, shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
    
    cmd = "http -a %s:%s POST :8000/air_quality/threshold/ threshold=%s" %(user, 
                                                                     password, 
                                                                     threshold)
    FNULL = open(os.devnull, 'w')
    subprocess.call(cmd, shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
    
    cmd = "http -a %s:%s POST :8000/air_quality/toalert/ alert_user=%s" %(user, 
                                                                password, 
                                                                users[0])
    FNULL = open(os.devnull, 'w')
    subprocess.call(cmd, shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
    
    cmd = "http -a %s:%s POST :8000/air_quality/toalert/ alert_user=%s" %(user, 
                                                                 password, 
                                                                users[1])
    subprocess.call(cmd, shell=True, stdout=FNULL, stderr=subprocess.STDOUT)    

def custom_event(user, password, value, point):
    param = get_db_data('param', 'pollutants')
    threshold = get_db_data('threshold', 'thresholdvalues')
    alert_user = get_db_data('alert_user', 'toalert')
    
    cmd = '''http -a %s:%s POST :8000/air_quality/monitor/ value=%s point="%s" alert_user=%s param=%s threshold=%s''' %(user, 
                                                                                                                  password, value, 
                                                                                                                  point, alert_user[0][0], 
                                                                                                                  param[0][0], threshold[0][0])
    FNULL = open(os.devnull, 'w')
    subprocess.call(cmd, shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
    
    cmd = '''http -a %s:%s POST :8000/air_quality/monitor/ value=%s point="%s" alert_user=%s param=%s threshold=%s''' %(user, 
                                                                                                                  password, value, 
                                                                                                                  point, alert_user[1][0], 
                                                                                                                  param[0][0], threshold[0][0])
    subprocess.call(cmd, shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
    

def ten_random_events(user, password, point):
    param = get_db_data('param', 'pollutants')
    threshold = get_db_data('threshold', 'thresholdvalues')
    alert_user = get_db_data('alert_user', 'toalert')
    
    for i in range(10):
        value = random.randint(10000, 30000)

        cmd = '''http -a %s:%s POST :8000/air_quality/monitor/ value=%i point="%s" alert_user=%s param=%s threshold=%s''' %(user, 
                                                                                                                      password, value, 
                                                                                                                      point, alert_user[0][0], 
                                                                                                                  param[0][0], threshold[0][0])
        FNULL = open(os.devnull, 'w')
        subprocess.call(cmd, shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
        
        cmd = '''http -a %s:%s POST :8000/air_quality/monitor/ value=%i point="%s" alert_user=%s param=%s threshold=%s''' %(user, 
                                                                                                                      password, value, 
                                                                                                                      point, alert_user[1][0], 
                                                                                                                  param[0][0], threshold[0][0])
        subprocess.call(cmd, shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
        
        
def alerts(user, password, useralert, coord):
    delete('alert')
    values = get_db_data('value', 'monitor')
    values_ids = get_db_data('id', 'monitor')
    threshold = threshold = get_db_data('threshold', 'thresholdvalues')
    
    dict_values = {}
    num_values = []
    for i in range(len(values)):
        num_values.append(int(values[i][0]))
        dict_values[num_values[i]] = values_ids[i][0]
    
    toalert_values = []
    for i in range(len(num_values)):
        if num_values[i] < int(threshold[0][0]):
            pass
        else:
            toalert_values.append(dict_values[num_values[i]])

    toalert_values = list(set(toalert_values))
    print toalert_values
    
    for i in range(len(toalert_values)):
        print toalert_values[i]
        print ((toalert_values[i])-1)
        cmd = 'http -a %s:%s POST :8000/air_quality/alert/ monitor_params=%i' %(user, password, toalert_values[i])
        FNULL = open(os.devnull, 'w')
        subprocess.call(cmd, shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
        
        cmd = 'http -a %s:%s POST :8000/air_quality/alert/ monitor_params=%i' %(user, password, ((toalert_values[i])-1))
        subprocess.call(cmd, shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
        
    if coord == 0:
        cmd = 'http :8000/air_quality/alert/?search=%s' %(useralert)
        subprocess.call(cmd, shell=True)
    if useralert == 0:
        cmd = 'http :8000/air_quality/alert/?in_bbox=%s' %(coord)
        subprocess.call(cmd, shell=True)
    else:
        pass