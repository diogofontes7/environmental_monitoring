from __future__ import unicode_literals

from django.apps import AppConfig


class AirQualityConfig(AppConfig):
    name = 'air_quality'
