# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

API Rest to monitor air quality

### How do I get set up? ###

**clone the repository

Install PostgreSQL and PostGIS

create db with the commands:**


```
#!python

createdb -U postgres airquality_db
psql -U postgres -d airquality_db -c "CREATE EXTENSION postgis;"
```

Install the requirements with pip:

django==1.10
djangorestframework==3.6.3
django-multiselectfield==0.1.7
django-filter==1.0.4
djangorestframework-gis==0.11.2
django-leaflet==0.22.0
httpie
psycopg2
subprocess
django_filters.rest_framework

example: 

```
#!python

pip install psycopg2
```


**To start**

Open a cmd go to the directory of the project in the manage.py directory and do the migrations to the db:

```
#!python

pyhton manage.py makemigrations
python manage.py migrate
```

After that create a superuser with

```
#!python

python manage.py createsuperuser
```


After that, start the local server (code was written to work with localhost:8000):

```
#!python

python manage.py runserver
```

To start using the api, open another cmd go to the same directory (of manage.py) and start the shell with:

```
#!python

python manage.py shell
```

Now we are ready to play with the API

First import these:
from add import add_rule, custom_event, ten_random_events, alerts

To make a rule (add the environmental parameter, the threshold and the list of users to alert) use this command:

```
#!python

add_rule('user', 'password', 'param', 'threshold', ('user1', 'user2'))
```
available params: CO, CO2, SO
available users: rvitorino, tbatista

To create a custom event with the value of the parameter and coordinates of the measurment use:

```
#!python

custom_event('user', 'password', 'value', "point")
```
where point should be in this format between " " as in the code
example: {'type': 'MultiPoint', 'coordinates': [[-8.43957139015198, 40.21699533967433]]}

To create ten random values events it's the same but with this:

```
#!python

ten_random_events('user', 'password', "point")
```

To see the alerts it can be queried by user or by location
By user use:

```
#!python

alerts('user', 'password', 'user to alert', 0)
```
where user to alert is 'rvitorino' ou 'tbatista'

By location use:

```
#!python

alerts('user', 'password', 0, "point")

```
where point is in the same format as before